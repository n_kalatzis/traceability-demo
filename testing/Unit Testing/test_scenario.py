"""
The following libraries are needed
    # pip install requests
    # pip install pytest pytest-html
    # pip install jsonschema
    # pip install pytest-asyncio

To execute the test , navigate into the folder where the "test_scenario.py" file resides and 
execute the following command : 
    $ pytest --tb=line -rP test_scenario.py

"""

import requests;
import pytest;
import json;
import os;
import datetime;
import sys;
from collections.abc import Mapping;

# CFG
printResponses = False; #This can greatly increase the verbosity level.
printCurls     = False; #This will print out every Curl executed in the unit tests.
debug          = False;

configuration = {
    "pie1" : {
        "register_kb" : {
            "knowledgeBaseId"         : "http://www.example.org/ploutos-app",
            "knowledgeBaseName"       : "Ploutos Traceability App",
            "knowledgeBaseDescription": "This app offers traceability info"
        },
        "register_ask" : {
            "knowledgeInteractionType" : "AskKnowledgeInteraction",
            "graphPattern"             : "?endProduct <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://www.tno.nl/agrifood/ontology/ploutos/common#Product> . ?output <https://www.tno.nl/agrifood/ontology/ploutos/common#intermediateProductOf> ?endProduct . ?operation <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?operationType . ?operationType <http://www.w3.org/2000/01/rdf-schema#subClassOf> <https://www.tno.nl/agrifood/ontology/ploutos/common#ProductOperation> . ?operation <http://www.w3.org/ns/ssn/hasInput> ?input . ?operation <http://www.w3.org/ns/ssn/hasOutput> ?output . ?operation <https://www.tno.nl/agrifood/ontology/ploutos/common#responsibleAgent> ?agent . ?agent <http://www.w3.org/2004/02/skos/core#prefLabel> ?agentName . ?operation <http://semanticweb.cs.vu.nl/2009/11/sem/hasTimeStamp> ?timestamp ."
        },
        "kb_ask" : [{ 
         "endProduct" : "<http://sense-web.neuropublic.gr:8686/lots/130821A-L4-181011>;" 
        }],
        "wrong_request" : {
         "wrong_lot_id" : {
          "kb_ask"   : [{ 
           "endProduct" : "<http://sense-web.neuropublic.gr:8686/lots/130821A-L4-18101>;" 
          }]
         },
         "no_lot_id" : {
          "kb_ask" : [{ 
           "endProduct" : "<http://sense-web.neuropublic.gr:8686/lots/>;" 
          }]
         }
        },
        "res"  : {
         "KnowledgeInteractionId" : ""
        },
        "port" : 8280,
        "url"  : { 
          "register_kb"  : "http://192.168.1.108:8280/rest/sc" , 
          "register_ask" : "http://192.168.1.108:8280/rest/sc/ki" , 
          "kb_ask"       : "http://192.168.1.108:8280/rest/sc/ask"
        },
        "responseValidation" : {
         "kb_ask"       : None , 
         "register_kb"  : None ,
         "register_ask" : "" ,
         "wrong_lot_id" : { "bindingSet" : [] , "exchangeInfo" : [] },
         "no_lot_id"    : { "bindingSet" : [] , "exchangeInfo" : [] }
        }
    },
    "prdd" : {
        "url" : "http://192.168.1.108:8080/servicedirectory/rest/directory/service"
    }
}

def logFile( myString ) : 
    myCurrentTime = datetime.datetime.now();
    myPrefix = "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : ";

    fd = open( os.path.abspath( "./log.log" ) , "a+");
    fd.write( myPrefix + str( myString ) + "\n");
    fd.close();

def formatCurl( request ):
    headers = [ "'{0}: {1}'".format(k, v.replace( "\"" , "\\\"" )) for k, v in request.headers.items() ];
    headers = " --header ".join( headers );
    if request.body is not None :
        mySanitizedMessage = " -d '" + str( request.body.replace( "\"" , "\\\"" ) + "'" );
    else:
        mySanitizedMessage = "";

    return ( "curl -X " + str( request.method ) + 
                " --header " + headers + mySanitizedMessage + " \"" + str( request.url ) + "\""
              );

def validateResponse( expectedResponse , receivedResponse ):

    if expectedResponse is None : 
        return { "success" : "No issues in validation" };

    myParsedResponse = receivedResponse;
    if isinstance( expectedResponse, Mapping ) or isinstance( expectedResponse, list ):
        try : 
            myParsedResponse = json.loads( receivedResponse );
        except Exception as error :
            return { "error" : "Response not a valid JSON" };
    else:
        if isinstance( receivedResponse, str ):
            return { "success" : "No issues in validation" };
        else:
            return { "error" : "Expected response should be a String not an Object." };

    if isinstance( expectedResponse, Mapping ):
        if isinstance( myParsedResponse, Mapping ) == False:
            return { "error" : "Expected response should be an Object." };
        for property in expectedResponse:
            if( property not in myParsedResponse ) :
                return { "error" : "Missing response property ( " + str( property ) + " )" };

    if isinstance( expectedResponse, list ):
        if isinstance( myParsedResponse, list ) == False:
            return { "error" : "Expected response should be a List." };
        for property in expectedResponse[ 0 ]:
            if( property not in myParsedResponse[ 0 ] ) :
                return { "error" : "Missing response property ( " + str( property ) + " )" };

    return { "success" : "No issues in validation" };

def resPrint( type , cfg , request ):
    if( type == "kb_addition" ):
        print( "Knowledge Base has been added : " + str( cfg[ "register_kb" ][ "knowledgeBaseId" ] ) );
    elif( type == "kb_exists" ):
        print( "Knowledge Base already exists : " + str( cfg[ "register_kb" ][ "knowledgeBaseId" ] ) );
    elif( type == "kb_removed" ):
        print( "Knowledge Base has been removed : " + str( cfg[ "register_kb" ][ "knowledgeBaseId" ] ) );
    elif( type == "ask_addition" ):
        print( "ASK has been added" );
        print( "Knowledge-Interaction-Id : " + str( cfg[ "res" ][ "KnowledgeInteractionId" ] ) );
    elif( type == "ask_exists" ):
        print( "ASK already exists : " + str( cfg[ "register_kb" ][ "knowledgeBaseId" ] ) );
    elif( type == "ask_missing_id" ):
        print( "ASK Failed. KB ID is unknown. : " + str( cfg[ "register_kb" ][ "knowledgeBaseId" ] ) );
    elif( type == "ask_removed" ):
        print( "ASK has been removed");
        print( "Knowledge-Interaction-Id : " + str( cfg[ "res" ][ "KnowledgeInteractionId" ] ) );
    elif( type == "ask_query" ):
        print( "ASK has been completed");
    elif( type == "ask_error" ):
        print( "ASK has error");
    else:
        print( "Unassigned type in console logging" );
    
    if printCurls is True : 
        print( "\n---CURL---\n" );
        print( formatCurl( request ) );

def queryPieASK( pie ):
    myResponse = requests.post( 
        url     = pie[ "url" ][ "kb_ask" ] , 
        data    = json.dumps( pie[ "kb_ask" ] ) , 
        headers = { 
          'Content-Type'             : 'application/json' , 
          'Knowledge-Base-Id'        : pie[ "register_kb" ][ "knowledgeBaseId" ] , 
          'Knowledge-Interaction-Id' : pie[ "res" ][ "KnowledgeInteractionId" ]
        }
    );
    if( debug == True ) : 
        logFile( "queryPieASK [ " + pie[ "url" ][ "kb_ask" ] + " ] : " );
        logFile( formatCurl( myResponse.request ) );
        logFile( "" );

    if( myResponse.status_code == 200 ):
        if printResponses == True : 
            print( "ASK Response" );
            print( myResponse.text );
            print();
        resPrint( "ask_query" , pie , myResponse.request );

        myValidation = validateResponse( pie[ "responseValidation" ][ "kb_ask" ] , myResponse.text );
        if( "error" in myValidation ) :
            return ( myValidation[ "error" ] );
        else:
            return True;

        return True;
    elif( myResponse.status_code == 404 and myResponse.text == "Could not add knowledge interaction, because the given knowledge base ID is unknown." ):
        resPrint( "ask_error" , pie , myResponse.request );
        return True;
    else:
        return ( "Status Code " + str ( myResponse.status_code ) + " for '" + pie[ "url" ][ "register_ask" ] + "'" );

def queryPieASK_error( pie , type ):
    myResponse = requests.post( 
        url     = pie[ "url" ][ "kb_ask" ] , 
        data    = json.dumps( pie[ "wrong_request" ][ type ][ "kb_ask" ] ) , 
        headers = { 
          'Content-Type'             : 'application/json' , 
          'Knowledge-Base-Id'        : pie[ "register_kb" ][ "knowledgeBaseId" ] , 
          'Knowledge-Interaction-Id' : pie[ "res" ][ "KnowledgeInteractionId" ]
        }
    );
    if( debug == True ) : 
        logFile( "queryPieASK_error [ " + pie[ "url" ][ "kb_ask" ] + " ] : " );
        logFile( formatCurl( myResponse.request ) );
        logFile( "" );

    if( myResponse.status_code == 200 ):
        if printResponses == True : 
            print( "ASK Response" );
            print( myResponse.text );
            print();

        resPrint( "ask_query" , pie , myResponse.request );

        myValidation = validateResponse( pie[ "responseValidation" ][ type ] , myResponse.text );
        if( "error" in myValidation ) :
            return ( myValidation[ "error" ] );
        else:
            return True;
    elif( myResponse.status_code == 404 and myResponse.text == "Could not add knowledge interaction, because the given knowledge base ID is unknown." ):
        resPrint( "ask_error" , pie , myResponse.request );
        return True;
    else:
        return ( "Status Code " + str ( myResponse.status_code ) + " for '" + pie[ "url" ][ "kb_ask" ] + "'" );

def registerPieASK( pie ):
    myResponse = requests.post( 
        url     = pie[ "url" ][ "register_ask" ] , 
        data    = json.dumps( pie[ "register_ask" ] ) , 
        headers = { 
          'Content-Type'      : 'application/json' , 
          'Knowledge-Base-Id' : pie[ "register_kb" ][ "knowledgeBaseId" ]
        }
    );
    if( debug == True ) : 
        logFile( "registerPieASK [ " + pie[ "url" ][ "register_ask" ] + " ] : " );
        logFile( formatCurl( myResponse.request ) );
        logFile( "" );

    if( myResponse.status_code == 200 ):
        myParsedResponse = json.loads( myResponse.text );
        pie[ "res" ][ "KnowledgeInteractionId" ] = myParsedResponse[ "knowledgeInteractionId" ];
        resPrint( "ask_addition" , pie  , myResponse.request );

        myValidation = validateResponse( pie[ "responseValidation" ][ "register_ask" ] , myResponse.text );
        if( "error" in myValidation ) :
            return ( myValidation[ "error" ] );
        else:
            return True;

        return True;
    elif( myResponse.status_code == 400 ):
        resPrint( "ask_exists" , pie , myResponse.request );
        return True;
    elif( myResponse.status_code == 404 and myResponse.text == "Could not add knowledge interaction, because the given knowledge base ID is unknown." ):
        resPrint( "ask_missing_id" , pie , myResponse.request );
        return True;
    else:
        return ( "Status Code " + str ( myResponse.status_code ) + " for '" + pie[ "url" ][ "register_ask" ] + "'" );

def registerPieKB( pie ):
    myResponse = requests.post( 
        url     = pie[ "url" ][ "register_kb" ] , 
        data    = json.dumps( pie[ "register_kb" ] ) , 
        headers = { 'content-type': 'application/json' }
    );
    if( debug == True ) : 
        logFile( "registerPieKB [ " + pie[ "url" ][ "register_kb" ] + " ] : " );
        logFile( formatCurl( myResponse.request ) );
        logFile( "" );

    if( myResponse.status_code == 200 ):
        resPrint( "kb_addition" , pie , myResponse.request );

        myValidation = validateResponse( pie[ "responseValidation" ][ "register_kb" ] , myResponse.text );
        if( "error" in myValidation ) :
            return ( myValidation[ "error" ] );
        else:
            return True;

        return True;
    elif( myResponse.status_code == 400 ):
        resPrint( "kb_exists" , pie , myResponse.request );
        return True;
    else:
        return ( "Status Code " + str ( myResponse.status_code ) + " for '" + pie[ "url" ][ "register_kb" ] + "'" );

def test_register_knowledge_base_on_pie():
    try:
        myResult = registerPieKB( configuration[ "pie1" ] );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_delete_knowledge_base_from_pie():
    try:
        myResponse = requests.delete( 
            url     = configuration[ "pie1" ][ "url" ][ "register_kb" ] , 
            headers = { 
                'content-type'      : 'application/json' , 
                'Knowledge-Base-Id' : configuration[ "pie1" ][ "register_kb" ][ "knowledgeBaseId" ] 
            }
        );
        if( debug == True ) : 
            logFile( "test_delete_knowledge_base_from_pie [ " + configuration[ "pie1" ][ "url" ][ "register_kb" ] + " ] : " );
            logFile( formatCurl( myResponse.request ) );
            logFile( "" );

        if( myResponse.status_code == 200 ):
            resPrint( "kb_removed" , configuration[ "pie1" ] , myResponse.request );
            assert True;
        else:
            assert False , "Status Code is : " + str( myResponse.status_code );

    except Exception as error :
        logFile( error );
        logFile( "" );
        assert False , error;

def test_register_same_knowledge_base_on_pie():
    try:
        myResult = registerPieKB( configuration[ "pie1" ] );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_register_ASK_on_pie():
    try:
        myResult = registerPieASK( configuration[ "pie1" ] );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_delete_ASK_on_pie():
    try:
        myResponse = requests.delete( 
            url     = configuration[ "pie1" ][ "url" ][ "register_ask" ] , 
            headers = { 
                'content-type'             : 'application/json' , 
                'Knowledge-Base-Id'        : configuration[ "pie1" ][ "register_kb" ][ "knowledgeBaseId" ] , 
                'Knowledge-Interaction-Id' : configuration[ "pie1" ][ "res" ][ "KnowledgeInteractionId" ] 
            }
        );
        if( debug == True ) : 
            logFile( "test_delete_ASK_on_pie [ " + configuration[ "pie1" ][ "url" ][ "register_ask" ] + " ] : " );
            logFile( formatCurl( myResponse.request ) );
            logFile( "" );

        if( myResponse.status_code == 200 ):
            resPrint( "ask_removed" , configuration[ "pie1" ] , myResponse.request );
            assert True;
        else:
            assert False , ( "Status Code is : " + str( myResponse.status_code ) + 
                             " | EndPoint : " + str( myResponse.url ) + 
                             " | Headers : " + 
                              str( "'content-type' : 'application/json'" ) +  
                              str( "'Knowledge-Base-Id'        : " + configuration[ "pie1" ][ "register_kb" ][ "knowledgeBaseId" ] ) +  
                              str( "'Knowledge-Interaction-Id' : " + configuration[ "pie1" ][ "res" ][ "KnowledgeInteractionId" ] ) 
                           );

    except Exception as error :
        assert False , error;

def test_register_ASK_on_pie_again():
    try:
        myResult = registerPieASK( configuration[ "pie1" ] );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_query_PIE_for_results():
    try:
        myResult = queryPieASK( configuration[ "pie1" ] );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_query_PIE_for_results_error_invalidLotId():
    try:
        myResult = queryPieASK_error( configuration[ "pie1" ] , "wrong_lot_id" );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;

def test_query_PIE_for_results_error_missingLotId():
    try:
        myResult = queryPieASK_error( configuration[ "pie1" ] , "no_lot_id" );
        if( myResult is True ):
            assert True;
        else:
            assert False , myResult;
    except Exception as error :
        assert False , error;
