# Unit Testing

### Programming Language
Python 3.6.5

### Libraries Needed
- pip install requests
- pip install pytest pytest-html
- pip install jsonschema

### Execution 
1. Follow the procedure described in the "testing/" README in order to Start the PIE
2. Download the "**test_scenario.py**" from this GIT folder
3. Navigate to the folder where "**test_scenario.py**" resides
4. Execute the following command in your Console
```bash
pytest --tb=short -rfP test_scenario.py
```
5. The output of each test will be displayed in your screen
