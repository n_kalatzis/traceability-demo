
import { createStore } from 'framework7/lite';

const store = createStore({
  state: {
    token: '',
    user: {},
    lot: {},
    soil_operations: '',
    active_processing_step: null,
    soil_operation: null,
    processing_index: 0,
    certificates: []
  },
})
export default store;
