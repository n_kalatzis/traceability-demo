# Ploutos Traceability Demo

This project contains the Ploutos Traceability app, and a demonstration environment for the Ploutos Interoperability Enabler, with Knowledge Mappers connected to several web services.

## Running it locally

To demo the app locally, do the following

1. Set up the local subdomains `frontend.localhost` and `backend.localhost` to point to 127.0.0.1:
   - Edit `/etc/hosts` if on Linux/macOS, or `C:\Windows\System32\drivers\etc\hosts` if on Windows
2. Start all necessary services:
```
docker-compose up --build --force-recreate -d nginx-local prdd app app-pie app-km app-km-processor gaiasense-km gaiasense-pie alterra-km alterra-pie certificate-km certificate-pie
```
3. If it's not yet in the database, create the default `processor` and `consumer` users in the Django app with the correct permissions:
   - First, create the superuser with `docker-compose exec app python manage.py createsuperuser`
   - Then, visit http://backend.localhost:8001/admin in your web browser and log in with the new superuser account
   - Go to `Groups`, and create a `processor` and a `consumer` group.
   - Go to `Users`, and create a `processor` and a `consumer` user.
   - Put both users in their respective groups, by selecting the respective group at `Groups` in the user page, and clicking `Save` (all the way at the bottom).
4. Visit http://frontend.localhost:8001 in your web browser.
5. Log in as `processor` or `consumer`.
6. Enter lot id `190721A-L02-0081`.
