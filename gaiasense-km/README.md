# GaiaSense Knowledge Mapper

This service is responsible for mapping the data from the GaiaSense API
(sense-web.neuropublic.gr:8585/) into terms of the Ploutos Common Semantic Model
(PCSM), and to disclose it to the Ploutos Interoperability Enabler (PIE).

## How is the Knowledge Mapper configured?

It is implemented as a configured plugin to the [Knowledge Mapper
project](https://ci.tno.nl/gitlab/tke/knowledge-mapper). In
[`config.json`](./config.json) you can see the configuration for this knowledge
mapper. The `plugin` property tells the Knowledge Mapper that it should make an
instance of the given Python class, with the given variables. The Python class
is in [`gaiasense_data_source.py`](./gaiasense_data_source.py). This Python
class should implement the methods of the [`DataSource`
class](https://ci.tno.nl/gitlab/tke/knowledge-mapper/-/blob/main/src/data_sources.py),
most notably the `handle` method, which receives incoming bindings and must
return the outgoing bindings with the data from the knowledge base (the
GaiaSense API). [^1]

### Knowledge interaction

The knowledge interactions (capabilities) of this knowledge base are also
configured in [`config.json`](./config.json). There are currently three
knowledge interactions, which provide the following knowledge:

- Coordinates of parcels
- Crop types of crops cultivated at parcels
- Pesticide operations ('sprays') at parcels

In [`config.json`](./config.json), each knowledge interaction is given:

- The type of knowledge interaction: In these cases they're all providing data
  reactively, so the interaction type should be `"answer"`.
- A [graph
  pattern](https://www.w3.org/TR/sparql11-query/#sparqlBasicGraphPatterns),
  which describes, in terms of the PCSM, what knowledge is provided.
- A list of all variables used in the pattern. (This is a technical detail that
  can be omited later.)
- A name. This is not required, but makes it easier to detect which knowledge
  interaction you're dealing with in the `handle` method.

[^1]: This process of extending a Docker images and adding your config+plugin is
  a bit involved, and it is probably better to be able to install the
  `knowlege-mapper` Python code through `pip`, but that's currently not
  possible.
