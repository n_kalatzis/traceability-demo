from rest_framework.authtoken import views as authtoken_views
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


class ObtainAuthToken(authtoken_views.ObtainAuthToken):
    """Class to obtain the AuthToken"""

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, _ = Token.objects.get_or_create(user=user)

        data = {"token": token.key, "user_id": user.pk}
        return Response(data)
