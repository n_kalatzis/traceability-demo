import logging as log
from datetime import datetime
from knowledge_mapper.tke_client import TkeClient
from knowledge_mapper import knowledge_interaction
import os
from core.utils.pie import kbs, FormattedCertificationData


KE_ENDPOINT_DEFAULT = "http://app-pie:8280/rest"
if 'KE_ENDPOINT' not in os.environ:
    log.warning(f"Please set the KE_ENDPOINT environment variable to configure where to find the PIE. Using the default '{KE_ENDPOINT_DEFAULT}' now.")  
KE_ENDPOINT = os.environ.get('KE_ENDPOINT', KE_ENDPOINT_DEFAULT)


class PIEService:
    def ask_for_certificates(self, user_type, organization):
        kb_id = kbs[user_type]
        query = [{"organisation": f"{organization}"}]
        bindings = ask(kb_id, 'ask-global-gap-number', query)
        return bindings 


    def ask_for_certificate_details(self, user_type, certificate):
        kb_id = kbs[user_type]
        query = [
            {
                "asoiId": f"{certificate}",
                "asoiProvider": "<https://www.tno.nl/agrifood/ontology/ploutos/sip1#globalGap>"
            }
        ]      
        bindings = ask(kb_id , 'ask-certificates', query)
        if len(bindings) > 0:
            return FormattedCertificationData(bindings[0]).get_formatted_data()      
        else:
            return None

    def ask_for_lot(self, lot_id, user_type):
        kb_id = kbs[user_type]
        query = [
            {"endProduct": f"<http://sense-web.neuropublic.gr:8686/lots/{lot_id}>"}]

        bindings = ask(kb_id, 'ask-chain', query)

        steps_with_lot_id = []
        for step in bindings:
            if str(lot_id) in step["endProduct"]:
                steps_with_lot_id.append(step)

        final_response = []
        lst_operations = []
        for step in steps_with_lot_id:
            operation_type = step["operationType"].split(
                "common#")[1].split(">")[0]
            if operation_type not in lst_operations:
                date = datetime.fromisoformat(
                    step["timestamp"].split('Z"^^')[0].split('"')[1]
                )

                resp = None
                operation_name = ""
                if operation_type == "HarvestingOperation":
                    operation_name = "Harvesting"
                    resp = self.ask_for_harvesting_operations(
                        user_type,
                        step["operation"]
                    )
                elif operation_type == "PackagingOperation":
                    operation_name = "Packaging"
                    resp = self.ask_for_packaging_operations(
                        user_type,
                        step["operation"]
                    )

                certificate_details = []
                certificates = self.ask_for_certificates(user_type, step["agent"])
                if certificates:
                     for certificate in certificates:
                        if "ggn" in certificate:
                            certificate_details.append(self.ask_for_certificate_details(user_type, certificate["ggn"]))    

                final_response.append({
                    "operation_type": operation_type,
                    "operation_name": operation_name,
                    "company": step["agentName"],
                    "date": date.strftime("%Y-%m-%d %H:%M:%S"),
                    "certificates": certificate_details
                })

                for key, value in resp.items():
                    final_response[len(lst_operations)][key] = value

                lst_operations.append(operation_type)

        return sorted(final_response, key=lambda x: x["date"], reverse=False)

    def ask_for_harvesting_operations(self, user_type, operation):
        kb_id = kbs[user_type]
        query = [{"operation": f"{operation}"}]

        bindings = ask(kb_id, 'ask-harvesting-operations', query)
        first_binding = bindings[0]

        data = {}
        data["farm"] = {
            "name": first_binding["farmName"],
            "association": first_binding["associationName"],
            "area": f'{round(float(first_binding["area"]), 3)} m²',
            "location": f'https://www.google.com/maps/@{first_binding["latitude"]},{first_binding["longitude"]},17z'
            # "map": {
            #     'latitude': response["latitude"],
            #     'longitude': response["longitude"]
            # }
        }
        data["crop"] = {
            "crop_name": first_binding["cropName"],
            "crop_type": first_binding["cropType"].split("obo/")[1].split(">")[0]
        }
        data["country"] = first_binding["countryLabel"]
        data["purpose"] = first_binding["purpose"]

        if user_type == "processor":
            data['parcel'] = {
                'operation': first_binding["parcel"],
                'toponym': first_binding["parcelToponym"],
                'region': first_binding["parcelRegion"],
                'processed by': first_binding["parcelAdminName"]
            }
        return data

    def ask_for_packaging_operations(self, user_type, operation):
        kb_id = kbs[user_type]
        query = [{"operation": f"{operation}"}]

        bindings = ask(kb_id, 'ask-packaging-operations', query)
        first_binding = bindings[0]

        return {
            # "agent": response["agent"].split(".org/")[1].split(">")[0],
            "country": '"Greece"',
            "location": f'https://www.google.com/maps/@{first_binding["longitude"]},{first_binding["latitude"]},17z'
        }

    def ask_for_soil_operations(self, user_type, operation):
        kb_id = kbs[user_type]
        query = [{"parcel": f"{operation}"}]
        bindings = ask(kb_id, 'ask-soil-operations', query)

        soil_operations_types = []
        operations = {}
        for soil_operation in bindings:
            operation_type = soil_operation["soilManagementOperationType"].split("common#")[
                1].split(">")[0]
            if operation_type not in soil_operations_types:
                soil_operations_types.append(operation_type)
                operations[operation_type] = []

        for soil_operation in bindings:
            operation_type = soil_operation["soilManagementOperationType"].split(
                "common#")[1].split(">")[0]
            operations[operation_type].append(
                {
                    "start": soil_operation["start"].split('"^^')[0].split('"')[1],
                    "end": soil_operation["end"].split('"^^')[0].split('"')[1],
                    "active substance": soil_operation['activeSubstance'],
                    "area value": f'{round(float(soil_operation["areaValue"]), 1)} l m²',
                    "applied value": f'{round(float(soil_operation["appliedValue"]), 1)} l m²'
                }
            )

        for soil_operation in operations:
            operations[soil_operation] = sorted(
                operations[soil_operation], key=lambda x: x["start"], reverse=False)

        return [operations]


def ask(kb_id, ki_name, bindings=[]):
    tke_client = TkeClient(KE_ENDPOINT)
    tke_client.connect()

    kb = tke_client.get_knowledge_base(kb_id)
    ki: knowledge_interaction.AskKnowledgeInteraction = kb.get_ki(name=ki_name)

    if ki is None:
        log.warn(
            f'Knowledge Interaction with name "{ki_name}" not found. Has the Knowledge Mapper registered it?'
        )
        bindings = []
    else:
        bindings = ki.ask(bindings)["bindingSet"]

    return bindings
