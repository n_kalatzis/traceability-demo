from enum import Enum

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class UserTypeEnum(Enum):
    consumer = 'Consumer'
    processor = 'Processor'


class User(User):
    user_type = models.CharField(max_length=15,
                                 choices=[(lvl.value, lvl.value)
                                          for lvl in UserTypeEnum]
                                 )

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return self.username
