import re

kbs = {
    'processor': 'https://alterra.org/peach-processing-kb',
    'consumer': 'http://www.example.org/ploutos-app'
}

graph_patherns_payloads = {
    'chain_operations': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "ssn": "http://www.w3.org/ns/ssn/",
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
            "s4agri": "https://saref.etsi.org/saref4agri/"
        },
        "graphPattern": "?endProduct rdf:type ploutos:Product . ?output ploutos:intermediateProductOf ?endProduct . ?operation rdf:type ?operationType . ?operationType rdfs:subClassOf ploutos:ProductOperation . ?operation ploutos:hasResponsibleAgent ?agent . ?agent s4agri:hasName ?agentName . ?operation ssn:hasInput ?input . ?operation ssn:hasOutput ?output . ?operation ploutos:hasEndDatetime ?timestamp ."
    },
    'packaging_operations': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
            "wgs84": "http://www.w3.org/2003/01/geo/wgs84_pos#",
            "foaf": "http://xmlns.com/foaf/0.1/"
        },
        "graphPattern": "?operation rdf:type ploutos:PackagingOperation . ?operation ploutos:hasResponsibleAgent ?agent . ?agent wgs84:location ?location . ?location wgs84:lat ?latitude . ?location wgs84:long ?longitude . ?agent ploutos:hasCountry ?country . ?country foaf:name ?countryLabel ."
    },
    'harvesting_operations': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "ssn": "http://www.w3.org/ns/ssn/",
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
            "s4agri": "https://saref.etsi.org/saref4agri/",
            "wgs84": "http://www.w3.org/2003/01/geo/wgs84_pos#",
            "foaf": "http://xmlns.com/foaf/0.1/"
        },
        "graphPattern": "?operation rdf:type ploutos:HarvestingOperation . ?operation ssn:hasOutput ?output . ?operation ploutos:isOperatedOn ?parcel . ?parcel s4agri:contains ?crop . ?crop rdf:type ?cropType . ?cropType rdfs:label ?cropName . ?parcel ploutos:hasArea ?area . ?farm rdf:type ploutos:Farm . ?farm wgs84:location ?location . ?location wgs84:lat ?latitude . ?location wgs84:long ?longitude . ?farm s4agri:hasName ?farmName . ?farm s4agri:contains ?parcel . ?farmer s4agri:managesFarm ?farm . ?association rdf:type ploutos:FarmAssociation . ?farmer s4agri:isMemberOf ?association . ?association s4agri:hasName ?associationName . ?parcel ploutos:hasAdministrator ?parcelAdminName . ?parcel ploutos:hasToponym ?parcelToponym . ?parcel ploutos:inRegion ?parcelRegion . ?output ploutos:isMeantFor ?purpose . ?parcel ploutos:hasCultivator ?cultivatorName . ?farm ploutos:hasCountry ?country . ?country foaf:name ?countryLabel ."
    },
    'soil_operations': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
            "om": "http://www.ontology-of-units-of-measure.org/resource/om-2/"
        },
        "graphPattern": "?operation rdf:type ?soilManagementOperationType . ?soilManagementOperationType rdfs:subClassOf ploutos:SoilManagementOperation . ?operation ploutos:isOperatedOn ?parcel . ?operation ploutos:hasAppliedAmount ?applied . ?applied om:hasUnit ?appliedUnit . ?applied om:hasNumericalValue ?appliedValue . ?operation ploutos:isAppliedPer ?area . ?area om:hasUnit ?areaUnit . ?area om:hasNumericalValue ?areaValue . ?operation ploutos:hasStartDatetime ?start . ?operation ploutos:hasEndDatetime ?end . ?operation ploutos:responsibleAgent ?agent . ?operation ploutos:usesMaterial ?material . ?material ploutos:hasActiveSubstance ?activeSubstance ."
    },
    'global_gap_number': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#"
        },
        "graphPattern": "?organisation ploutos:hasGlobalGapNumber ?ggn ."
    },
    'certificates': {
        "knowledgeInteractionType": "AskKnowledgeInteraction",
        "prefixes": {
            "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#"
        },
        "graphPattern": "?certificate rdf:type ploutos:Certificate . ?certificate ploutos:hasAuthoritySpecificIdentifier ?asci . ?asci ploutos:hasIdentifier ?asciId . ?asci ploutos:isProvidedBy ?asciProvider . ?certificate ploutos:hasCertifiedOrganization ?organisation . ?organisation ploutos:hasAuthoritySpecificIdentifier ?asoi . ?asoi ploutos:hasIdentifier ?asoiId . ?asoi ploutos:isProvidedBy ?asoiProvider . ?certificate ploutos:hasCertificationScheme ?scheme . ?certificate ploutos:isValidFrom ?validFrom . ?certificate ploutos:isValidUntil ?validUntil . ?certificate ploutos:hasCertifiedObject ?productClass ."
    }
}

CERTIFICATION_LABELS = {
    "scheme" : "Certification",
    "asoiId": "Organization identifier",
    "asciId": "Certification number",
    "validityPeriod": "Validity Period",
    "certificateSource": "Certification document"
}

class FormattedFrontData():
    labels = {}
    data = None
    formatted_data = []

    def __init__(self, labels, data):
        self.labels = labels
        self.data = data
        self.formatted_data = []

    def get_formatted_data(self):
        for key in self.data.keys():
            if key in self.labels and self.data[key] and self.data[key] != "":
                new_value = self.data_modification(key, self.data[key], self.labels)
                self.formatted_data.append({"label": self.labels[key], "value": new_value})
        return self.formatted_data

    def data_modification(self, key, data):
        return data            

class FormattedCertificationData(FormattedFrontData):

    def __init__(self, data):
        data["validityPeriod"] = self.clean_string(data["validFrom"]) + " - " + self.clean_string(data["validUntil"])
        super(FormattedCertificationData, self).__init__(CERTIFICATION_LABELS, data)
    
    def data_modification(self, key, data, labels):
        if key == "certificateSource":
            return self.get_link(data, labels[key])
        elif key != "validityPeriod":
            return self.clean_string(data)
        else:
            return data    

    def clean_string(self, data):
        data = re.findall(r'\"(.+?)\"', data)
        if len(data) > 0:
            return data[0]
        else:
            return ""

    def get_link(self, data, text):
        data = re.findall(r'<(.*)>', data)
        if len(data) > 0:
            return data[0]
        else:
            return ""
        

   



