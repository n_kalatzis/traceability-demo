from core.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ploutos',
        'USER': 'development',
        'PASSWORD': 'development',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
