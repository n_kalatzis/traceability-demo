from core.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ploutos',
        'USER': 'admin_ploutos',
        'PASSWORD': 'super_ploutos_password1234',
        'HOST': 'db',
        'PORT': '5432',
    }
}
